defmodule GraphqlServerWeb.Shop do
  @shops [
    %{
      id: 1,
      name: "Clothing Shop 1",
      category: :CLOTHING,
      address: %{
        country: "Argentina",
        city: "Córdoba"
      }
    },
    %{
      id: 2,
      name: "Clothing Shop 2",
      category: :CLOTHING,
      address: %{
        country: "Argentina",
        city: "Tilcara"
      }
    },
    %{
      id: 3,
      name: "Clothing Shop 3",
      category: :HARDWARE,
      address: %{
        country: "Chile",
        city: "Osorno"
      }
    },
    %{
      id: 4,
      name: "Clothing Shop 4",
      category: :HARDWARE,
      address: %{
        country: "Chile",
        city: "Santiago de Chile"
      }
    }
  ]

  def all(%{category: category}) do
    case Enum.filter(@shops, &(&1.category === category)) do
      [] -> {:error, %{message: "not found", details: %{category: category}}}
      shops -> {:ok, shops}
    end
  end

  def all(_) do
    {:ok, @shops}
  end

  def find(%{id: id}) do
    case Enum.find(@shops, &(&1.id === id)) do
      nil -> {:error, %{message: "not found", details: %{id: id}}}
      shop -> {:ok, shop}
    end
  end

  def update(id, params) do
    with {:ok, shop} <- find(%{id: id}) do
      {:ok, Map.merge(shop, params)}
    end
  end
end
